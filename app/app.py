from flask import Flask, render_template, request ,flash, redirect
from packages.topsis import task

app = Flask(__name__)
ALLOWED_EXTENSIONS = set(['txt', 'csv'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/calcula', methods=['POST'])
def calcula_topsis():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            topsis_result, nomes = task.taskTopsis(file)
            chart, topsis_table = task.plot_chart_and_table_pygal(nomes, topsis_result)
            return render_template('topsis_result.html', topsis_table=topsis_table, chart=chart )
    return redirect(request.url)

if __name__ == '__main__':
    app.run()


