from packages.topsis.TOPSIS import TopSis
import pandas as pd
import pygal



def taskTopsis(filename):
    topsis = TopSis(filename)
    matriz_decisao = topsis.matriz_decisao
    size = matriz_decisao.shape
    [num_alternativas, num_criterios] = size
    pesos = topsis.pesos
    normalizada = topsis.normaliza_matriz(matriz_decisao, num_alternativas, num_criterios, size)
    normalizada_com_pesos = topsis.introduz_pesos(normalizada, pesos)
    max = normalizada_com_pesos.max(axis=0)
    min = normalizada_com_pesos.min(axis=0)
    solucoes_ideal_pos , solucoes_ideal_neg  = topsis.get_solucoes_ideais(num_criterios, max, min, topsis.custo_ou_beneficio)
    diferenca_positiva, diferenca_negativa = topsis.calcula_distancia_euclidiana(normalizada_com_pesos,solucoes_ideal_pos,
                                                               solucoes_ideal_neg, num_alternativas, num_criterios)
    resultado = topsis.calcula_proximidade_relativa(num_alternativas, diferenca_positiva, diferenca_negativa)

    return ( pd.DataFrame(resultado) , topsis.nomes)

def topsis_to_table_html(topsis_result):
    return topsis_result.to_html(classes=["table"])

def topsis_bar_chart(resultado:pd.DataFrame, nomes):
    bar_chart = pygal.Bar()  # Then create a bar graph object
    for index, row in resultado.iterrows():
        bar_chart.add(nomes[index], row.get_values())  # Add some values
    return bar_chart


def plot_chart_and_table_pygal(nomes, topsis_result):
    chart = topsis_bar_chart(topsis_result, nomes)
    chart = chart.render_data_uri()
    topsis_table = topsis_bar_chart(topsis_result, nomes)
    topsis_table = topsis_table.render_table(style=True, transpose=True)
    return chart, topsis_table


