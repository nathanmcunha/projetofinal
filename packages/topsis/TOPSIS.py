
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import numpy as np
import pandas as pd



class TopSis:

    def __init__(self, file):

        try:
            if self.get_extension(file) == "csv":
                self.carrega_csv(file)
            else:
                self.carrega_txt(file)

        except IOError:
            print("Problemas para abrir o arquivo")
            raise IOError


    def carrega_csv(self, file):
        self.data = pd.read_csv(file)

        self.pesos = self.data.iloc[0].values
        self. pesos = np.array(self.pesos[1:])
        if self.pesos.sum() != 1.0:
            print('ERROR: the sum of the weights must be 1')
            raise ValueError
        self.custo_ou_beneficio = self.data.iloc[1].values
        self.custo_ou_beneficio = np.array(self.custo_ou_beneficio[1:]).astype(int)


        indice = list(self.data.columns.values)[0]
        self.data = self.data.drop(self.data.index[[0, 1]])
        self.data.set_index(indice)
        self.nomes = self.data[indice].values
        dropado = self.data.drop(labels=indice, axis=1)
        self.matriz_decisao = np.array(dropado.values)
        self.size = self.matriz_decisao.shape
        [self.num_alternativas, self.num_criterios] = self.size


    def carrega_txt(self, file):
        self.data = np.loadtxt(file, dtype=float)
        self.pesos = self.get_pesos(self.data)
        if self.pesos.sum() != 1.0:
            print('ERROR: the sum of the weights must be 1')
            raise ValueError
        self.custo_ou_beneficio = self.get_custo_ou_beneficio(self.data)
        self.matriz_decisao = self.get_matriz_decisao(self.data)
        self.size = self.matriz_decisao.shape
        [self.num_alternativas, self.num_criterios] = self.size

    def get_extension(self, file):
       return file.filename.split('.')[-1]


    @classmethod
    def get_pesos(cls, data):
        return data[0, :]

    @classmethod
    def is_soma_pesos_equal_um(cls, pesos):
        if np.asarray(pesos).sum() != 1.0:
            return False
        return True

    @classmethod
    def get_custo_ou_beneficio(cls, data):
        return data[1, :].astype(int)

    @classmethod
    def get_matriz_decisao(cls, data):
        return data[2:, :]

    @classmethod
    def normaliza_matriz(cls, matriz_decisao, num_alternativas, num_criterios, size):
        normalizada = np.zeros(size, dtype=float)
        m = matriz_decisao ** 2
        m = np.sqrt(m.sum(axis=0))
        for i in range(num_alternativas):
            for j in range(num_criterios):
                normalizada[i, j] = matriz_decisao[i, j] / m[j]
        return normalizada

    @classmethod
    def introduz_pesos(cls, normalizada, pesos):
        normalizada_com_pesos = normalizada * pesos
        return np.asarray(normalizada_com_pesos)

    @classmethod
    def get_solucoes_ideais(cls, num_criterios, max, min, custo_ou_beneficio):
        ideal_positiva = np.zeros(num_criterios, dtype=float)
        ideal_negativa = np.zeros(num_criterios, dtype=float)

        for j in range(num_criterios):
            if custo_ou_beneficio[j] == 1:
                ideal_positiva[j] = min[j]
                ideal_negativa[j] = max[j]
            elif custo_ou_beneficio[j] == 0:
                ideal_positiva[j] = max[j]
                ideal_negativa[j] = min[j]
            else:
                print('ERROR: O valor dos pesos deve ser 1 OU 0')
                raise ValueError
        return ideal_positiva, ideal_negativa

    @classmethod
    def calcula_distancia_euclidiana(cls, normalizada_com_pesos, ideal_positiva, ideal_negativa, numero_alternativas,
                                     numero_criterios):

        distancia_positiva = np.zeros(numero_alternativas, dtype=float)
        distancia_negativa = np.zeros(numero_alternativas, dtype=float)
        for i in range(numero_alternativas):
            for j in range(numero_criterios):
                distancia_positiva[i] = distancia_positiva[i] + cls.distancia \
                    (normalizada_com_pesos[i, j], ideal_positiva[j])
                distancia_negativa[i] = distancia_negativa[i] + cls.distancia(
                    normalizada_com_pesos[i, j], ideal_negativa[j])
        distancia_positiva = np.sqrt(distancia_positiva)
        distancia_negativa = np.sqrt(distancia_negativa)

        return distancia_positiva, distancia_negativa

    @classmethod
    def calcula_proximidade_relativa(cls, numero_alternativas, distancia_positiva, distancia_negativa):
        proximidade_relativa = np.zeros(numero_alternativas, dtype=float)
        for i in range(numero_alternativas):
            d_negativa = distancia_negativa[i]
            proximidade_relativa[i] = d_negativa / (distancia_positiva[i] + d_negativa)
        return proximidade_relativa

    @classmethod
    def distancia(cls, a, b):
        return (a - b) ** 2


